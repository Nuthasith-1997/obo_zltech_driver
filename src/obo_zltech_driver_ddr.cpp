#include <ros/ros.h>

//#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include "obo_zltech_driver/MotorInput.h"
//#include "obo_zltech_driver/MotorData.h"

#define WHEEL_TRACK 0.315
#define WHEEL_RADIUS 0.055
#define RAD_TO_RPM 9.5493

//ros::Publisher odom_pub;
//nav_msgs::Odometry odom_msg;
ros::Publisher rpm_pub;
obo_zltech_driver::MotorInput rpm_msg;

//volatile double past_time;
//volatile double dt;

/*void odomCallback(const obo_zltech_driver::MotorData::ConstPtr &msg)
{


    odom_msg.header.seq++;
    odom_msg.header.stamp = ros::Time::now();
    odom_msg.pose.pose.position.x
    odom_msg.pose.pose.position.y
    odom_msg.pose.pose.orientation.x
    odom_msg.pose.pose.orientation.y
    odom_msg.pose.pose.orientation.z
    odom_msg.pose.pose.orientation.w
    odom_msg.twist.twist.linear.x
    odom_msg.twist.twist.linear.y
    odom_msg.twist.twist.angular.z

    odom_pub.publish(odom_msg);
}*/

void velCallback(const geometry_msgs::Twist::ConstPtr &msg)
{   
    ROS_INFO_STREAM("Recieved cmd_vel");
    rpm_msg.mot_input_left = (-RAD_TO_RPM)*((2*msg->linear.x - msg->angular.z*WHEEL_TRACK)/(2*WHEEL_RADIUS));
    rpm_msg.mot_input_right = RAD_TO_RPM*((2*msg->linear.x + msg->angular.z*WHEEL_TRACK)/(2*WHEEL_RADIUS));
    rpm_pub.publish(rpm_msg);
}

int main(int argc, char* argv[])
{
    ROS_INFO_STREAM("Initialize obo_zltech_driver_ddr node");
    ros::init(argc, argv, "obo_zltech_driver_ddr");
    ros::NodeHandle nh;
    
    /*odom_msg.header.seq = 0;
    odom_msg.header.stamp = ros::Time::now();
    odom_msg.header.frame_id = "odom";
    odom_msg.child_frame_id = "wheel_base";

    past_time = ros::Time::now().toSec();
    dt = 0.0;*/

    /*ros::Subscriber mot_sub;
    mot_sub = nh.subscribe<nav_msgs::Odometry>("/motors_data", 10, odomCallback);
    odom_pub = nh.advertise<nav_msgs::Odometry>("/wheel_odom", 10);
    */
    ros::Subscriber vel_sub;
    vel_sub = nh.subscribe<geometry_msgs::Twist>("/cmd_vel", 10, velCallback);
    rpm_pub = nh.advertise<obo_zltech_driver::MotorInput>("/motors_input", 10);
}