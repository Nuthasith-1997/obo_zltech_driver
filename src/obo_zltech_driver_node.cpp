#include "obo_zltech_driver/obo_zltech_driver_node.h"

void inputCallback(const obo_zltech_driver::MotorInput::ConstPtr &msg)
{
    switch(cm){
        case 0:
            ROS_INFO_STREAM("Control Mode is not defined");
            break;
        case 1:
            ROS_INFO_STREAM("Relative Position Mode is not available");
            break;
        case 2:
            ROS_INFO_STREAM("Absolute Position Mode is not available");
            break;
        case 3:
            // Velocity Mode

            if (!zltech.setVelocityTarget(id, msg->mot_input_left, msg->mot_input_right)){
                ROS_ERROR_STREAM("Cannot set target velocities.");
            }
            break;
        case 4:
            // Torque Mode

            if (!zltech.setTorqueTarget(id, msg->mot_input_left, msg->mot_input_right)){
                ROS_ERROR_STREAM("Cannot set target velocities.");
            }
            break;
    }
}

bool setModeCallback(obo_zltech_driver::SetMode::Request &req, obo_zltech_driver::SetMode::Response &res)
{
    switch (req.mode)
    {
    case 3:
        setVelocityMode();
        break;
    case 4:
        setTorqueMode();
        break;
    case 5:
        clearError();
        break;
    default:
        ROS_INFO_STREAM("No specified mode");
        break;
    }
    return true;
}

int main(int argc, char* argv[])
{
    ros::init(argc, argv, "obo_zltech_driver_node");
    ros::NodeHandle nh;
    ros::Rate rate(60);
    
    zltech.init("/dev/ttyUSB0", 115200, 'N', 1);
    if(!zltech.connect())
        return -1;
    setVelocityMode();
    
    ros::Subscriber mot_input_sub;
    mot_input_sub = nh.subscribe<obo_zltech_driver::MotorInput>("/motors_input", 10, inputCallback);

    obo_zltech_driver::MotorData mot_msg;
    ros::Publisher motor_pub;
    motor_pub = nh.advertise<obo_zltech_driver::MotorData>("/motors_data", 10);

    ros::ServiceServer set_mode_srv;
    set_mode_srv = nh.advertiseService("/set_mode", setModeCallback);
    
    while(ros::ok()){
        rate.sleep();
        if (!zltech.getVelocity(id, velocity)){
            ROS_ERROR_STREAM("Cannot get velocities.");
        }
        if (!zltech.getCurrent(id, current)){
            ROS_ERROR_STREAM("Cannot get currents.");
        }
        if (!zltech.getMotTemp(id, mot_temp)){
            ROS_ERROR_STREAM("Cannot get motor temperatures.");
        }
        if (!zltech.getDriverTemp(id, &driver_temp)){
            ROS_ERROR_STREAM("Cannot get driver temperature.");
        }
        if (!zltech.getDCVoltage(id, &voltage)){
            ROS_ERROR_STREAM("Cannot get driver voltage.");
        }
        if (!zltech.getErrorCode(id, error)){
            ROS_ERROR_STREAM("Cannot get error codes.");
        }

        mot_msg.dc_volt = voltage;
        mot_msg.temperature_left = mot_temp[0];
        mot_msg.temperature_right = mot_temp[1];
        mot_msg.error_left = error[0];
        mot_msg.error_right = error[1];
        mot_msg.velocity_left = velocity[0];
        mot_msg.velocity_right = velocity[1];
        mot_msg.current_left = current[0];
        mot_msg.current_right = current[1];

        motor_pub.publish(mot_msg);

        ros::spinOnce();
    }
}

void setVelocityMode(void){
    ROS_INFO_STREAM("Setting velocity mode...");
    cm = VelocityMode;
    cw = Stop;
    if (!zltech.setControlWord(id, cw)){
        ROS_ERROR_STREAM("Canno set control word");
    }
    if (!zltech.setControlMode(id, cm)){
        ROS_ERROR_STREAM("Cannot set mode");
    }
    if (!zltech.setAccTime(id, acc_time)){
        ROS_ERROR_STREAM("Cannot set Acc time");
    }
    if (!zltech.setDecTime(id, dec_time)){
        ROS_ERROR_STREAM("Cannot set Dec time");
    }
    cw = Enable;
    if (!zltech.setControlWord(id, cw)){
        ROS_ERROR_STREAM("Canno set control word");
    }
}

void setTorqueMode(void){
    ROS_INFO_STREAM("Setting torque mode...");
    cm = TorqueMode;
    cw = Stop;
    if (!zltech.setControlWord(id, cw)){
        ROS_ERROR_STREAM("Canno set control word");
    }
    if (!zltech.setControlMode(id, cm)){
        ROS_ERROR_STREAM("Cannot set mode");
    }
    if (!zltech.setAccTime(id, acc_time)){
        ROS_ERROR_STREAM("Cannot set Acc time");
    }
    if (!zltech.setDecTime(id, dec_time)){
        ROS_ERROR_STREAM("Cannot set Dec time");
    }
    cw = Enable;
    if (!zltech.setControlWord(id, cw)){
        ROS_ERROR_STREAM("Canno set control word");
    }   
}

void clearError(void){
    ROS_INFO_STREAM("Clearing errors...");
    cm = Undefined;
    cw = Stop;
    if (!zltech.setControlWord(id, cw)){
        ROS_ERROR_STREAM("Canno set control word");
    }
    if (!zltech.setControlMode(id, cm)){
        ROS_ERROR_STREAM("Cannot set mode");
    }
    cw = ClearFaults;
    if (!zltech.setControlWord(id, cw)){
        ROS_ERROR_STREAM("Cannot set control word");
    }
}