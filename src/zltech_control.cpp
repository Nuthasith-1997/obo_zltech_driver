#include "obo_zltech_driver/zltech_control.h"

ZltechControl::ZltechControl(){
    ROS_INFO("[ZltechControl] Class Initialized");
}

ZltechControl::~ZltechControl(){}

bool ZltechControl::setControlMode(uint8_t id, uint16_t control_mode){
    std::cout << "ZltechControl::setControlMode(" << (int)id << ") : " << control_mode << std::endl;
    return writeRegister(id, CONTROL_MODE, control_mode);
}

bool ZltechControl::setControlWord(uint8_t id, uint16_t control_word){
    std::cout << "ZltechControl::setControlWord(" << (int)id << ") : " << control_word << std::endl;
    return writeRegister(id, CONTROL_WORD, control_word);
}

bool ZltechControl::setAccTime(uint8_t id, uint16_t value){
    std::cout << "ZltechControl::setAccTime(" << (int)id << ") : " << value << std::endl;
    return writeRegister(id, ACC_TIME_LEFT, value) && writeRegister(id, ACC_TIME_RIGHT, value);
}

bool ZltechControl::setDecTime(uint8_t id, uint16_t value){
    std::cout << "ZltechControl::setDecTime(" << (int)id << ") : " << value << std::endl;
    return writeRegister(id, DECEL_TIME_LEFT, value) && writeRegister(id, DECEL_TIME_RIGHT, value);   
}

bool ZltechControl::setTorqueRate(uint8_t id, uint16_t value){
    std::cout << "ZltechControl::setTorqueRate(" << (int)id << ") : " << value << std::endl;
    return writeRegister(id, TORQUE_RATE_LEFT, value) && writeRegister(id, TORQUE_RATE_RIGHT, value);   
}

bool ZltechControl::setTorqueTarget(uint8_t id, int16_t value_left, int16_t value_right){
    //std::cout << "ZltechControl::setTorqueTarget(" << (int)id << ")" << " : ";
    //std::cout << "Left : " << value_left << " , ";
    //std::cout << "Right : " << value_right << std::endl;
    return writeRegister(id, TARGET_TORQUE_LEFT, value_left) && writeRegister(id, TARGET_TORQUE_RIGHT, value_right);
}

bool ZltechControl::setVelocityTarget(uint8_t id, int16_t value_left, int16_t value_right){
    //std::cout << "ZltechControl::setVelocityTarget(" << (int)id << ")" << " : ";
    //std::cout << "Left : " << value_left << " , ";
    //std::cout << "Right : " << value_right << std::endl;
    return writeRegister(id, TARGET_VEL_LEFT, value_left) && writeRegister(id, TARGET_VEL_RIGHT, value_right);
}

bool ZltechControl::getVelocity(uint8_t id, int16_t* dest){
    // x0.1 rpm
    uint16_t buffer;
    bool ret = readRegisters(id, ACTUAL_VEL_LEFT, 1, &buffer);
    *dest = buffer;

    bool ret2 = readRegisters(id, ACTUAL_VEL_RIGHT, 1, &buffer);
    *(dest+1) = buffer;
    return ret && ret2;
}

bool ZltechControl::getCurrent(uint8_t id, int16_t* dest){
    // x0.1 A
    uint16_t buffer;
    bool ret = readRegisters(id, ACTUAL_CURR_LEFT, 1, &buffer);
    *dest = buffer;

    bool ret2 = readRegisters(id, ACTUAL_CURR_RIGHT, 1, &buffer);
    *(dest+1) = buffer;
    return ret && ret2;
}

bool ZltechControl::getMotTemp(uint8_t id, uint8_t* dest){
    // 1 deg C
    // High 8 bits = Left
    // Low 8 bits = Right
    uint16_t dest_buffer;
    uint8_t value_buffer = 0;
    bool ret = readRegisters(id, MOT_TEMP, 1, &dest_buffer);
    value_buffer = dest_buffer >> 8;
    *dest = value_buffer;
    value_buffer = dest_buffer;
    *(dest+1) = value_buffer;
    return ret;
}

bool ZltechControl::getDriverTemp(uint8_t id, int16_t* dest){
    uint16_t buffer;
    bool ret = readRegisters(id, DRIVER_TEMP, 1, &buffer);
    *dest = buffer;
    return ret; 
}

bool ZltechControl::getDCVoltage(uint8_t id, uint16_t* dest){
    // x0.01 V
    uint16_t buffer;
    bool ret = readRegisters(id, DC_VOLT, 1, &buffer);
    *dest = buffer;
    return ret;
}

bool ZltechControl::getErrorCode(uint8_t id, uint16_t* dest){
    uint16_t buffer;
    bool ret = readRegisters(id, ERROR_LEFT, 1, &buffer);
    *dest = buffer;

    bool ret2 = readRegisters(id, ERROR_RIGHT, 1, &buffer);
    *(dest+1) = buffer;
    return ret && ret2;
}