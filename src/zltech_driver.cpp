#include "obo_zltech_driver/zltech_driver.h"
#include <stdlib.h>
#include <sstream>

ZltechDriver::ZltechDriver()
{
    ROS_INFO("[ZltechDriver] Class Initialized");
    modbus_ = NULL;
}

ZltechDriver::~ZltechDriver(){}

bool ZltechDriver::init(std::string port_name, uint32_t baud_rate, char parity, int data_bit, int stop_bit)
{
    ROS_INFO("[ZltechDriver] initialize serial %s \n", port_name.c_str());
    // modbus_ = modbus_new_ascii(port_name.c_str(), baud_rate, 'N', 8, 1);
    port_name_  = port_name;
    baud_rate_  = baud_rate;
    parity_     = parity;
    data_bit_   = data_bit;
    stop_bit_   = stop_bit;
    return true;
}

bool ZltechDriver::setByteTimeout(uint32_t byte_timeout_sec, uint32_t byte_timeout_usec)
{
    int ret = 0;
    ret = modbus_set_byte_timeout(modbus_, byte_timeout_sec, byte_timeout_usec);
    if(ret == -1)
    {
        return false;
    }
    uint32_t byte_to_sec;
    uint32_t byte_to_usec;
    modbus_get_byte_timeout(modbus_, &byte_to_sec, &byte_to_usec);
    ROS_INFO("[ZltechDriver] Byte Timeout: %u %u\n", byte_to_sec, byte_to_usec);
    return true;
}

bool ZltechDriver::setResponseTimeout(uint32_t response_timeout_sec, uint32_t response_timeout_usec)
{
    int ret = 0;
    ret = modbus_set_response_timeout(modbus_, response_timeout_sec, response_timeout_usec);
    if(ret == -1)
    {
        return false;
    }
    uint32_t response_to_sec;
    uint32_t response_to_usec;
    modbus_get_response_timeout(modbus_, &response_to_usec, &response_to_usec);
    ROS_INFO("[ZltechDriver] Response Timeout: %u %u\n", response_to_sec, response_to_usec);
    return true;
}

// bool        setId(uint8_t id, uint8_t new_id);
// bool        setModelName(uint8_t id, std::string name);
// std::string getModelName(uint8_t id);
// bool        scan(uint8_t *get_id, uint8_t *get_id_num, uint8_t range = 200);
// bool        ping(uint8_t id, uint16_t *get_model_number);
// bool        reboot(uint8_t id);
// bool        reset(uint8_t id);

bool ZltechDriver::connect()
{
    try
    {
        ROS_INFO("[ZltechDriver] connect");
        modbus_ = modbus_new_rtu(port_name_.c_str(), baud_rate_, parity_, data_bit_, stop_bit_);
        if (NULL == modbus_)
        {
            throw std::runtime_error(modbus_strerror(errno));
        }
        if (modbus_connect(modbus_) == -1)
        {
            modbus_close(modbus_);
            modbus_free(modbus_);
            throw std::runtime_error(modbus_strerror(errno));
            ROS_ERROR("Connection failed: %s\n", modbus_strerror(errno));
            return false;
        }
    }
    catch(const std::runtime_error& e)
    {
        std::ostringstream oss;
        oss << __PRETTY_FUNCTION__ << ": " << e.what();
        oss << " {devname = \"" << port_name_.c_str() << "\", baudrate = " << baud_rate_ << "}";
        std::cout << oss.str() << std::endl;
        return false;
        // throw std::runtime_error(oss.str());
    }
    return true;
}

bool ZltechDriver::disconnect()
{
    try
    {
        ROS_INFO("[ZltechDriver] disconnect");
        if(modbus_ != NULL)
        {
            modbus_close(modbus_);
        }
    }
    catch(const std::runtime_error& e)
    {
        std::ostringstream oss;
        oss << __PRETTY_FUNCTION__ << ": " << e.what();
        oss << " {devname = \"" << port_name_.c_str() << "\", baudrate = " << baud_rate_ << "}";
        // throw std::runtime_error(oss.str());
        return false;
    }
    return true;
}

bool ZltechDriver::writeBit(uint8_t id, int addr, uint8_t value)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_write_bit(modbus_, addr, value) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Write Bit");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::writeBits(uint8_t id, int addr, int nb, uint8_t *value)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_write_bits(modbus_, addr, nb, value) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Write Bits");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::readBits(uint8_t id, int addr, int nb, uint8_t *dest)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_read_bits(modbus_, addr, nb, dest) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Read Bits");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::readInputBits(uint8_t id, int addr, int nb, uint8_t *dest)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_read_input_bits(modbus_, addr, nb, dest) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Read Input Bits");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::writeRegister(uint8_t id, int addr, int value)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_write_register(modbus_, addr, value) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Write Register");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::writeRegisters(uint8_t id, int addr, int nb, uint16_t *value)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_write_registers(modbus_, addr, nb, value) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Write Registers");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}
bool ZltechDriver::readRegisters(uint8_t id, int addr, int nb, uint16_t *dest)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_read_registers(modbus_, addr, nb, dest) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Read Registers");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::readInputRegisters(uint8_t id, int addr, int nb, uint16_t *dest)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_read_input_registers(modbus_, addr, nb, dest) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Read Input Registers");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::writeReadRegisters(uint8_t id, int write_addr, int write_nb, uint16_t* value, int read_addr, int read_nb, uint16_t *dest)
{
    bool ret = true;
    if(selectId(id))
    {
        if (modbus_write_and_read_registers(modbus_, write_addr, write_nb, value, read_addr, read_nb, dest) < 0)
        {
            ROS_ERROR_STREAM("[ZltechDriver] Error Write And Read Registers");
            ret = false;
        }
    }
    else
    {
        ret = false;
    }
    return ret;
}

bool ZltechDriver::selectId(int id)
{
    bool ret = true;
    if (modbus_set_slave(modbus_, id) == -1)
    {
        ret = false;
    }
    return ret;  
}