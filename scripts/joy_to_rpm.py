#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from geometry_msgs.msg import Twist

def joyCallback(data):
    cmd_vel_msg.linear.x = data.axes[1] * 0.8
    cmd_vel_msg.angular.z = data.axes[3] * 1.2

    cmd_vel_pub.publish(cmd_vel_msg)

if __name__ == "__main__":
    rospy.init_node("joy_to_rpm")
    rospy.loginfo("Starting joy_to_rpm.")

    cmd_vel_msg = Twist()
    cmd_vel_pub = rospy.Publisher('/cmd_vel', Twist, queue_size=10)
    
    rospy.Subscriber('/joy', Joy, joyCallback)

    rospy.spin()
