#!/usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from obo_zltech_driver.msg import MotorInput

WHEEL_TRACK = 0.315
WHEEL_RADIUS = 0.055
RAD_TO_RPM = 9.5493

def velCallback(msg):
    motor_msg.mot_input_left = -RAD_TO_RPM*((2*msg.linear.x - msg.angular.z*WHEEL_TRACK)/(2*WHEEL_RADIUS))
    motor_msg.mot_input_right = RAD_TO_RPM*((2*msg.linear.x + msg.angular.z*WHEEL_TRACK)/(2*WHEEL_RADIUS))
    motor_pub.publish(motor_msg)

if __name__ == "__main__":
    rospy.init_node("ddr")
    rospy.loginfo("Starting ddr.")

    motor_msg = MotorInput()
    motor_pub = rospy.Publisher('/motors_input', MotorInput, queue_size=10)

    rospy.Subscriber('/cmd_vel', Twist, velCallback)
    
    rospy.spin()
