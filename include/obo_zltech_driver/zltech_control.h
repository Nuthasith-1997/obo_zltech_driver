#include "obo_zltech_driver/zltech_driver.h"
#include "obo_zltech_driver/zltech_item.h"

class ZltechControl : public ZltechDriver
{
public:
    ZltechControl();
    ~ZltechControl();

    // Common
    bool setControlMode(uint8_t id, uint16_t control_mode);
    bool setControlWord(uint8_t id, uint16_t control_word);

    // Control
    bool setAccTime(uint8_t id, uint16_t value);
    bool setDecTime(uint8_t id, uint16_t value);
    bool setVelocityTarget(uint8_t id, int16_t value_left, int16_t value_right);
    bool setTorqueRate(uint8_t id, uint16_t value);
    bool setTorqueTarget(uint8_t id, int16_t value_left, int16_t value_right);
    
    // Read Only
    bool getVelocity(uint8_t id, int16_t* dest);
    bool getCurrent(uint8_t id, int16_t* dest);
    bool getMotTemp(uint8_t id, uint8_t* dest);
    bool getDriverTemp(uint8_t id, int16_t* dest);
    bool getDCVoltage(uint8_t id, uint16_t* dest);
    bool getErrorCode(uint8_t id, uint16_t* dest);
};