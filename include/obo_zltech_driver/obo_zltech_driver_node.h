#include <stdint.h>

#include <ros/ros.h>

#include "obo_zltech_driver/MotorInput.h"
#include "obo_zltech_driver/MotorData.h"
#include "obo_zltech_driver/SetMode.h"

#include "obo_zltech_driver/zltech_control.h"

enum ControlMode {Undefined,
                  RelativePositionMode,
                  AbsolutePositionMode,
                  VelocityMode,
                  TorqueMode};

enum ControlWord {QuickStop = 5,
                  ClearFaults,
                  Stop,
                  Enable,
                  StartSynchronousPositionMode,
                  StartLeft,
                  StartRight};

ControlMode cm;
ControlWord cw;

ZltechControl zltech;

void setVelocityMode(void);
void setTorqueMode(void);
void clearError(void);

uint16_t acc_time = 100; //ms
uint16_t dec_time = 100;
uint16_t torque_rate = 300; //mA/s

uint8_t id = 1;
int16_t velocity[2] = {0, 0};
uint8_t mot_temp[2] = {0, 0};
int16_t driver_temp = 0;
int16_t current[2] = {0, 0};
uint16_t voltage = 0;
uint16_t error[2] = {0, 0};